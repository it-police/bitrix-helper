<?php

namespace ITPolice\Bitrix;

use ITPolice\Bitrix\CUtilEx as CUtil;

class Helper
{
    public static function translit($value = '', $lang = 'ru', $options = array("replace_other" => '-', "replace_space" => '-'))
    {
        return CUtil::translit($value, $lang, $options);
    }

    public static function phpToJs($value = '')
    {
        return CUtil::PhpToJSObject($value);
    }

    public static function clearBuffer() {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
    }

    public static function lazyLoadBefore(\CBitrixComponent $Component) {
        $bxajaxid = self::getComponentAjaxId($Component);
        if (array_key_exists('is_lazy_load', $_REQUEST) && $_REQUEST['is_lazy_load']=='y' && $_REQUEST['bxajaxid']==$bxajaxid) {
            self::clearBuffer();
        }

    }

    public static function lazyLoadAfter(\CBitrixComponent $Component) {
        $bxajaxid = self::getComponentAjaxId($Component);

        if ($Component->arParams["DISPLAY_BOTTOM_PAGER"]) {
            echo str_replace('#bxajaxid#', $bxajaxid, $Component->arResult["NAV_STRING"]);
        }
        if (array_key_exists('is_lazy_load', $_REQUEST) && $_REQUEST['is_lazy_load']=='y' && $_REQUEST['bxajaxid']==$bxajaxid) {
            endBeforeDie();
            die();
        }
    }

    public static function getComponentAjaxId(\CBitrixComponent $Component) {
        //dump($Component->__name, $Component->__template->__name);
        $bxajaxid = \CAjax::GetComponentID($Component->getName(), $Component->getTemplateName());
        return $bxajaxid;
    }
}