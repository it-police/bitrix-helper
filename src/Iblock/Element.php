<?php

namespace ITPolice\Bitrix\Iblock;

use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;

class Element
{

    public static function getId($filter = array())
    {
        Loader::includeModule('iblock');

        // Ищем раздел в БД по коду
        $res = \CIBlockElement::GetList(
            array(),
            $filter,
            false,
            array('ID'),
            array()
        );

        // Если не нашли добавляем в БД
        if ($res->SelectedRowsCount()) {
            $arItem = $res->GetNext();
            return $arItem['ID'];
        }

        return false;
    }

    public static function getItem($filter = array(), $select = array('ID'))
    {
        $items = self::getList($filter, $select, ["SORT"=>"ASC"], ["nTopCount"=>1], false);

        if(count($items)) {
            return $items[0];
        }

        return false;
    }

    public static function getList($filter = array(), $select = array('ID'), $sort = array("SORT"=>"ASC"), $arNavStartParams = false, $arGroupBy = false)
    {
        Loader::includeModule('iblock');

        // Ищем раздел в БД по коду
        $res = \CIBlockElement::GetList(
            $sort,
            $filter,
            $arGroupBy,
            $arNavStartParams,
            $select
        );

        $list = array();

        if ($res->SelectedRowsCount()) {
            while ($ob = $res->GetNextElement()) {
                $arItem = $ob->GetFields();
                $arItem['PROPS'] = $ob->GetProperties();
                $list[] = $arItem;
            }
        }

        return $list;
    }

    public static function add($data = array(), $bWorkFlow = false, $bUpdateSearch = true, $bResizePictures = false)
    {
        Loader::includeModule('iblock');

        $Element = new \CIBlockElement();

        // добавляем в БД
        $addRes = $Element->Add($data, $bWorkFlow, $bUpdateSearch, $bResizePictures);
        if (!$addRes) {
            Debug::dumpToFile($data, $Element->LAST_ERROR.' '.json_encode($data, JSON_UNESCAPED_UNICODE), PATH_LOG_FILE);
            throw new \Exception($Element->LAST_ERROR);
        }

        return $addRes;
    }

    public static function update($id, $data = array(), $bWorkFlow = false, $bUpdateSearch = true, $bResizePictures = false)
    {
        Loader::includeModule('iblock');

        $Element = new \CIBlockElement();

        // добавляем в БД
        $updateRes = $Element->Update($id, $data, $bWorkFlow, $bUpdateSearch, $bResizePictures);
        if (!$updateRes) {
            Debug::dumpToFile($data, $Element->LAST_ERROR.' '.json_encode($data, JSON_UNESCAPED_UNICODE), PATH_LOG_FILE);
            throw new \Exception($Element->LAST_ERROR);
        }

        return $updateRes;
    }

    public static function delete($id)
    {
        Loader::includeModule('iblock');

        return \CIBlockElement::Delete($id);
    }


    public static function updateProp($id, $iblock_id, $prop_val, $prop_code = false)
    {
        Loader::includeModule('iblock');
        $Element = new \CIBlockElement();
        $Element->SetPropertyValues($id, $iblock_id, $prop_val, $prop_code);
    }
}