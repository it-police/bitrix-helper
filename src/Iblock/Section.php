<?php

namespace ITPolice\Bitrix\Iblock;

use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;

class Section
{

    public static function getId($filter = array())
    {
        Loader::includeModule('iblock');

        // Ищем раздел в БД по коду
        $res = \CIBlockSection::GetList(
            array(),
            $filter,
            false,
            array('ID'),
            array()
        );

        if ($res->SelectedRowsCount()) {
            $arItem = $res->GetNext();
            return $arItem['ID'];
        }

        return false;
    }

    public static function getItem($filter = array(), $select = array('ID'))
    {
        Loader::includeModule('iblock');

        // Ищем раздел в БД по коду
        $res = \CIBlockSection::GetList(
            array(),
            $filter,
            false,
            $select,
            array()
        );

        if ($res->SelectedRowsCount()) {
            return $res->GetNext();
        }

        return false;
    }

    public static function getList($filter = array(), $select = array('ID'), $arOrder = [], $bIncCnt = false, $arNavStartParams = false)
    {
        Loader::includeModule('iblock');

        // Ищем раздел в БД по коду
        $res = \CIBlockSection::GetList(
            $arOrder,
            $filter,
            $bIncCnt,
            $select,
            $arNavStartParams
        );

        $list = array();

        if ($res->SelectedRowsCount()) {
            while ($arItem = $res->GetNext()) {
                $list[] = $arItem;
            }
        }

        return $list;
    }

    public static function add($data = array())
    {
        Loader::includeModule('iblock');

        $Section = new \CIBlockSection();

        // добавляем в БД
        $addRes = $Section->Add($data);
        if (!$addRes) {
            Debug::dumpToFile($data, $Section->LAST_ERROR.' '.json_encode($data, JSON_UNESCAPED_UNICODE), PATH_LOG_FILE);
            throw new \Exception($Section->LAST_ERROR);
        }

        return $addRes;
    }

    public static function update($id, $data = array())
    {
        Loader::includeModule('iblock');

        $Section = new \CIBlockSection();

        // добавляем в БД
        $updateRes = $Section->Update($id, $data);
        if (!$updateRes) {
            Debug::dumpToFile($data, $Section->LAST_ERROR.' '.json_encode($data, JSON_UNESCAPED_UNICODE), PATH_LOG_FILE);
            throw new \Exception($Section->LAST_ERROR);
        }

        return $updateRes;
    }


}