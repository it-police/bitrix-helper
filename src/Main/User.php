<?php

namespace ITPolice\Bitrix\Main;

use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;

class User
{

    public static function IsAuthorized()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            return true;
        }
        return false;
    }

    public static function getId()
    {
        if (self::IsAuthorized()) {
            global $USER;
            return $USER->GetID();
        }
        return false;
    }

    public static function getById($ID) {
        $rsUser = \CUser::GetByID($ID); // выбираем пользователей
        $arUser = $rsUser->Fetch();

        return $arUser;
    }

    public static function update($ID, $fields = []) {
        $user = new \CUser;
        if(!$user->Update($ID, $fields)) {
            throw new \Exception($user->LAST_ERROR);
            return false;
        }
        return true;
    }

    public static function create($fields = []) {
        $user = new \CUser;
        if(!$user->Add($fields)) {
            throw new \Exception($user->LAST_ERROR);
            return false;
        }

        return true;
    }

    public static function find($filter = [], $params = ['SELECT' => ['UF_*']]) {
        $elementsResult = \CUser::GetList(($by="ID"), ($order="DESC"), $filter, $params);

        $res = [];
        while ($arUser = $elementsResult->GetNext()){
            $res[] = $arUser;
        }

        return $res;
    }
    public static function findOne($filter) {
        $res = self::find($filter, [
            'SELECT' => ['UF_*'],
            'NAV_PARAMS' => array(
                'nTopCount' => 1
            ),
        ]);
        if(!empty($res))
            return $res[0];

        return false;
    }


    public static function addInGroup($userId, $groupId) {
        $arGroups = \CUser::GetUserGroup($userId);
        $arGroups[] = $groupId;
        \CUser::SetUserGroup($userId, $arGroups);
    }

    public static function login($phone, $password, $remember = true) {
        global $USER;
        if (!is_object($USER)) $USER = new \CUser();
        return $USER->Login($phone, $password, (boolean) $remember ? "Y" : "N");
    }
    public static function logout() {
        global $USER;
        return $USER->Logout();
    }

    public static function register($USER_LOGIN, $USER_NAME, $USER_LAST_NAME, $USER_PASSWORD, $USER_CONFIRM_PASSWORD, $USER_EMAIL) {
        global $USER;
        $res = $USER->Register($USER_LOGIN, $USER_NAME, $USER_LAST_NAME, $USER_PASSWORD, $USER_CONFIRM_PASSWORD, $USER_EMAIL);

        if($res['ID'])
            \CUser::SendUserInfo($res['ID'], SITE_ID, "Ваш пароль для входа: $USER_PASSWORD");

        return $res;
    }
}